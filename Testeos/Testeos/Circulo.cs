﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Testeos
{
    class Circulo : FiguraGeometrica
    {
        public double Radio { get; set; }

        public Circulo(double radio)
        {
            this.Radio = radio;
        }
        public override double Area()
        {
            return Math.PI*Math.Pow(Radio,2);
        }

        public override double Perimetro()
        {
            return Math.PI*Radio*2;
        }
    }
}
