﻿using System;

namespace Testeos
{
    public abstract class FiguraGeometrica
    {
        public override string ToString() => GetType().Name;
        public abstract double Area();
        public abstract double Perimetro();
    }
}
