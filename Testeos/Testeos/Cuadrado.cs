﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Testeos
{
    public class Cuadrado : FiguraGeometrica
    {
        public double Lado { get; set; }
       
        public Cuadrado(double lado)
        {
            this.Lado = lado;
        }
        public override double Area()
        {
            return Lado * Lado;
        }

        public override double Perimetro()
        {
            return Lado * 4;
        }

        public string Prueba()
        {
            return "hola";
        }
    }
}
